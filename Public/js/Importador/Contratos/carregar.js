function cadastrar(data, url) {
    data = JSON.stringify(data);
    var jsonAjax = new XMLHttpRequest();
    jsonAjax.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            postMessage(this.responseText);
        }
    };
    jsonAjax.open("POST", url, false);
    jsonAjax.setRequestHeader("Content-type", "application/json");
    jsonAjax.send(data);
}

function carregar() {
    var url = 'http://localhost:8000/api/';
    var classe = 'ControllerContratos';
    var metodo = 'show';

    postMessage('Caregando');
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var Contratos = JSON.parse(this.responseText);
            Contratos = Contratos.data;
            var max = Contratos.length;
            var i;

            for (i = 0; i < max; i++) {
                var perc = (100 * (i+1)) / max;
                var id = i + 1;
                if (perc >= 99.7) {
                    perc = 100;
                }

                var anexos = buscarAnexos(Contratos[i].id);

                var contrato = {
                    class: classe,
                    method: "save",
                    titulo: Contratos[i].resumo,
                    descricao: Contratos[i].descricao,
                    autor: 13,
                    data: Contratos[i].created_at,
                    numero: Contratos[i].numero,
                    ano: Contratos[i].ano,
                    tipo: Contratos[i].tipo,
                    anexos: anexos
                };
                cadastrar(contrato, url);
                postMessage('{"perc": "' + perc.toFixed(2) + '", "id": "' + id + '"}');
            }
            postMessage('FIM');
        }
    };
    xhttp.open("GET", url + "?class=" + classe + "&method=" + metodo, true);
    xhttp.send();
}

carregar();

function buscarAnexos(id) {
    var url = 'http://localhost:8000/api/';
    var classe = 'ControllerAnexo';
    var metodo = 'show';
    var http = new XMLHttpRequest();
    var resp = [];
    http.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var Anexos = JSON.parse(this.responseText);
            Anexos = Anexos.data;
            var max = Anexos.length;
            var i;

            for (i = 0; i < max; i++) {
                resp.push({
                    url: Anexos[i].arquivo_uid,
                    data: Anexos[i].created_at
                });
            }
        }
    };
    http.open("GET", url + "?class=" + classe + "&method=" + metodo + "&id=" + id, false);
    http.send();
    return resp;
}