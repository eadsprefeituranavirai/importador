perc = null;
id = null;
workerContratos = null;

$(function () {
    $('#btn-importar').on('click', function () {
        startWorker();
        $('#btn-importar').attr('disabled', 'disabled');
    });
});

var workerContratos;

function startWorker() {
    workerContratos = new Worker("Public/js/Importador/Contratos/carregar.js");
    workerContratos.onmessage = function (event) {
        if (event.data === 'Caregando') {
            $('#msg').toggleClass('hidden', false);
        } else {
            if (event.data == 'FIM') {
                stopWorker();
            } else {
                if (String(event.data).indexOf('{"perc":') >= 0) {
                    $('#msg').toggleClass('hidden', true);
                    gravar(JSON.parse(event.data));
                }
                // else {
                //     console.log(event.data);
                // }
            }
        }
    };
}

function stopWorker() {
    workerContratos.terminate();
    workerContratos = undefined;
    $('#btn-importar').removeAttr('disabled');
}

function gravar(obj) {
    $('#progresso').find('.sr-only-focusable').html(obj.perc + '%');
    $('#progresso').css('width', obj.perc + '%');
    $('#atual').html(obj.id);
}