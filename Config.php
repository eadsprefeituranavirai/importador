<?php


require './vendor/autoload.php';

define('SITE_URL', 'http://localhost:8000');
define('INPUT_SALVAR_CONFIG', '<input type="hidden" name="salvar_config" value="config">');

$talski = Config::carregarBancoTalski();
$wp = Config::carregarBancoWp();

define('NOME_TALSKI', $talski['nomeTalski']);
define('HOST_TALSKI', $talski['hostTalski']);
define('USER_TALSKI', $talski['userTalski']);
define('SENHA_TALSKI', $talski['senhaTalski']);

define('NOME_WP', $wp['nomeWp']);
define('HOST_WP', $wp['hostWp']);
define('USER_WP', $wp['userWp']);
define('SENHA_WP',$wp['senhaWp']);

define('LICITACAO', 1095);
define('ANEXOS_LICITACAO', 1100);

define('LEGISLACAO', 1987);
define('ANEXOS_LEGISLACAO', 1991);

define('CONTRATOS', 1986);
define('ANEXOS_CONTRATOS', 2003);


use App\Control\HomeControl;
use App\Templates\HomeTemplate;

class Config
{
    private static $CONFIG;

    public static function salvar($dados)
    {
        self::ini_put_contents('App/Database/Config.ini', $dados);
        $pg = new HomeControl();
        $pg->show('Salvo com sucesso!');
    }

    private static function carregar()
    {
        if (!isset(self::$CONFIG)) {
            if (file_exists('App/Database/Config.ini')) {
                self::$CONFIG = parse_ini_file('App/Database/Config.ini');
            } else {
                return null;
            }
        }
        return true;
    }

    public static function carregarBancoTalski()
    {
        if (self::carregar()) {
            return [
                'nomeTalski' => self::$CONFIG['nomeTalski'],
                'hostTalski' => self::$CONFIG['hostTalski'],
                'userTalski' => self::$CONFIG['userTalski'],
                'senhaTalski' => self::$CONFIG['senhaTalski'],
            ];
        }
        return null;
    }

    public static function carregarBancoWp()
    {
        if (self::carregar()) {
            return [
                'nomeWp' => self::$CONFIG['nomeWp'],
                'hostWp' => self::$CONFIG['hostWp'],
                'userWp' => self::$CONFIG['userWp'],
                'senhaWp' => self::$CONFIG['senhaWp'],
            ];
        }
        return null;
    }

    private static function ini_put_contents($inifile, $content)
    {
        $linhas = '';
        foreach ($content as $nome => $cont) {
            if (strlen($linhas) > 2) {
                $linhas .= "\n\n";
            }
            $linhas .= "[{$nome}] \n";
            foreach ($cont as $key => $value) {
                $linhas .= "{$key}={$value}\n";
            }
        }
        file_put_contents($inifile, $linhas);
    }
}