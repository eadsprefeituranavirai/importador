<?php

namespace App\Templates;

use Config;

class HomeTemplate
{
    private $view = [];

    public function mostrar($view)
    {
        $this->view = $view;
        include_once 'header.php';
//
        ?>
        <div class="container">
            <section class="col-md-12 body">
                <div class="page-header">
                    <h1><?php echo $view['titulo'] ?><br>
                        <small><?php echo $view['subTitulo'] ?></small>
                    </h1>
                </div>
                <?php if (isset($view['errorMessage'])): ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <strong>Erro!</strong><br>
                        <?php echo $view['errorMessage']; ?>
                    </div>
                <?php endif; ?>

                <?php if (isset($view['successMessage'])): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <strong>Sucesso!</strong><br>
                        <?php echo $view['successMessage']; ?>
                    </div>
                <?php endif; ?>

                <form method="POST" action="<?php echo SITE_URL.'/index.php'; ?>">

                    <?php echo INPUT_SALVAR_CONFIG ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Banco Talski</h3>
                        </div>
                        <div class="panel-body form-group">

                            <?php $talski = Config::carregarBancoTalski(); ?>

                            <div class="col-md-3">
                                <label for="nomeTalski">Nome do banco</label>
                                <input value="<?php echo $talski['nomeTalski']; ?>" class="form-control" type="text" required name="nomeTalski" id="nomeTalski">
                            </div>

                            <div class="col-md-3">
                                <label for="hostTalski">Host</label>
                                <input value="<?php echo $talski['hostTalski']; ?>" class="form-control" type="text" required name="hostTalski" id="hostTalski">
                            </div>

                            <div class="col-md-3">
                                <label for="userTalski">Usuário</label>
                                <input value="<?php echo $talski['userTalski']; ?>" class="form-control" type="text" required name="userTalski" id="userTalski">
                            </div>

                            <div class="col-md-3">
                                <label for="senhaTalski">Senha</label>
                                <input value="<?php echo $talski['senhaTalski']; ?>" class="form-control" type="text" required name="senhaTalski" id="senhaTalski">
                            </div>



                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Banco Wordpress</h3>
                        </div>
                        <div class="panel-body form-group">

                            <?php $wp = Config::carregarBancoWp(); ?>

                            <div class="col-md-3">
                                <label for="nomeWp">Nome do banco</label>
                                <input value="<?php echo $wp['nomeWp']; ?>" class="form-control" type="text" required name="nomeWp" id="nomeWp">
                            </div>

                            <div class="col-md-3">
                                <label for="hostWp">Host</label>
                                <input value="<?php echo $wp['hostWp']; ?>" class="form-control" type="text" required name="hostWp" id="hostWp">
                            </div>

                            <div class="col-md-3">
                                <label for="userWp">Usuário</label>
                                <input value="<?php echo $wp['userWp']; ?>" class="form-control" type="text" required name="userWp" id="userWp">
                            </div>

                            <div class="col-md-3">
                                <label for="senhaWp">Senha</label>
                                <input value="<?php echo $wp['senhaWp']; ?>" class="form-control" type="text" required name="senhaWp" id="senhaWp">
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12 text-right no-padding">
                        <button class="btn btn btn-success">Salvar</button>
                    </div>
                </form>
            </section>
            <!--            <section class="col-md-3 sidebar">-->
            <!--                <div id="feed"></div>-->
            <!--            </section>-->
        </div>
        <?php
        include_once 'footer.php';
    }

}