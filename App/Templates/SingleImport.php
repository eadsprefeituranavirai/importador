<?php

namespace App\Templates;

class SingleImport
{
    private $view = [];

    public function mostrar($view)
    {
        $this->view = $view;
        include_once 'header.php';
//
        ?>
        <div class="container">
            <section class="col-md-12 body" id="body">
                <input id="classe" type="hidden" value="<?php echo $view['classe']?>">
                <input id="metodo" type="hidden" value="<?php echo $view['metodo']?>">
                <div class="page-header">
                    <h1>
                        <img style="float: left;max-width: 86px;margin-right:  5px;" src="/Public/images/blog.png" alt="WordPress" class="img-responsive">
                        Importar <?php echo $view['titulo'] ?><br>
                        <small><?php echo $view['subTitulo'] ?></small>
                    </h1>
                </div>
                <?php if (isset($view['errorMessage'])): ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <strong>Erro!</strong><br>
                        <?php echo $view['errorMessage']; ?>
                    </div>
                <?php endif; ?>

                <?php if (isset($view['successMessage'])): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <strong>Sucesso!</strong><br>
                        <?php echo $view['successMessage']; ?>
                    </div>
                <?php endif; ?>

                <p>
                <h4 id="msg" class="hidden"><b>Coletando Informações...</b></h4>
                <br>
                <h4 class="text-center"><b>Total de <?php echo $view['titulo'] ?>: </b><?php echo $view['total']; ?></h4><br>
                    <div class="progress">
                        <div id="progresso" class="progress-bar progress-bar-success progress-bar-striped active text-center" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                             aria-valuemax="100" style="width:0%">
                            <span class="sr-only-focusable">0%</span>
                        </div>
                    </div>
                <h5><span id="atual">0</span><?php echo '/'.$view['total']; ?></h5>
                </p>

            <div class="col-md-12 text-right no-padding">
                <button class="btn btn btn-success" id="btn-importar">Importar</button>
            </div>
            </section>
<!--            <section class="col-md-3 sidebar">-->
<!--                <div id="feed"></div>-->
<!--            </section>-->
        </div>
        <?php
        include_once 'footer.php';
    }

}