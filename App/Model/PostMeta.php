<?php

namespace App\Model;

use \PDO;
use Importador\Database\Connection;

class PostMeta
{
    const METADATA_IMAGENS_DESTAQUE = 'a:5:{s:5:"width";i:960;s:6:"height";i:720;s:4:"file";s:18:"==imagem==";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"==imagem==";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"==tipo==";}s:6:"medium";a:4:{s:4:"file";s:18:"==imagem==";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"==tipo==";}s:12:"medium_large";a:4:{s:4:"file";s:18:"==imagem==";s:5:"width";i:768;s:6:"height";i:576;s:9:"mime-type";s:10:"==tipo==";}s:11:"slider-home";a:4:{s:4:"file";s:18:"==imagem==";s:5:"width";i:500;s:6:"height";i:300;s:9:"mime-type";s:10:"==tipo==";}s:11:"img-noticia";a:4:{s:4:"file";s:17:"==imagem==";s:5:"width";i:135;s:6:"height";i:74;s:9:"mime-type";s:10:"==tipo==";}s:14:"img-video-home";a:4:{s:4:"file";s:18:"==imagem==";s:5:"width";i:290;s:6:"height";i:160;s:9:"mime-type";s:10:"==tipo==";}s:18:"pg_imagem_destaque";a:4:{s:4:"file";s:18:"==imagem==";s:5:"width";i:420;s:6:"height";i:232;s:9:"mime-type";s:10:"==tipo==";}s:20:"pg_noticias_destaque";a:4:{s:4:"file";s:18:"==imagem==";s:5:"width";i:221;s:6:"height";i:159;s:9:"mime-type";s:10:"==tipo==";}s:18:"pg_imagens_galeria";a:4:{s:4:"file";s:18:"==imagem==";s:5:"width";i:327;s:6:"height";i:260;s:9:"mime-type";s:10:"==tipo==";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}';

    private $meta_id;
    private $post_id;
    private $meta_key;
    private $meta_value;

    public function __construct($post, $chave, $valor)
    {
        $this->meta_id = self::getLastId() + 1;
        $this->post_id = $post;
        $this->meta_key = $chave;
        $this->meta_value = $valor;
    }

    public static function getLastId()
    {
        $sql = 'SELECT max(meta_id) as max FROM wp_postmeta';
        $conn = Connection::getConnectionWP();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        return $data->max;
    }

    public function save()
    {
        $sql = "INSERT INTO wp_postmeta (meta_id, post_id, meta_key, meta_value) VALUES ('";
        $sql = $sql . $this->meta_id;
        $sql = $sql . "', '" . $this->post_id . "'";
        $sql = $sql . ", '" . $this->meta_key . "'";
        $sql = $sql . ", '" . $this->meta_value . "');";
        $conn = Connection::getConnectionWP();
        return $conn->exec($sql);
    }
}
