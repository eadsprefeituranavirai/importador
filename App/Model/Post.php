<?php

namespace App\Model;

use \PDO;
use Importador\Database\Connection;

class Post
{
    protected $id;
    protected $post_author;
    protected $post_date;
    protected $post_date_gmt;
    protected $post_content;
    protected $post_title;
    protected $post_name;
    protected $post_modified;
    protected $post_modified_gmt;
    protected $post_type;
    protected $post_mime_type;
    protected $post_status;

    public function __construct($titulo, $descricao, $autor, $post_date, $type)
    {
        $this->id = self::getLastId() + 1;
        $this->post_author = $autor;
        $this->post_date = $post_date;
        $this->post_title = $titulo;
        $this->post_content = $descricao;
        $this->post_name = $this->limparAcentos($titulo.$this->id);
        $this->post_modified = date('Y-m-d H:i:s');
        date_default_timezone_set('UTC');
        $this->post_date_gmt = date('Y-m-d H:i:s', strtotime($this->post_date));
        $this->post_modified_gmt = date('Y-m-d H:i:s', strtotime($this->post_modified));
        date_default_timezone_set('America/Campo_Grande');
        $this->post_type = $type;
        $this->post_mime_type = '';
        $this->post_status = '';

    }

    public function save()
    {
        $sql = $this->gerarSql();
        $conn = Connection::getConnectionWP();
        return $conn->exec($sql);

    }

    public function getLastId()
    {
        $sql = "SELECT max(ID) as max FROM wp_posts";
        $conn = Connection::getConnectionWP();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        $conn = null;
        return $data->max;
    }

    public static function obterUrlPadrao()
    {
        $sql = "SELECT option_value as url FROM wp_options WHERE option_id=1";
        $conn = Connection::getConnectionWP();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        $conn = null;
        return $data->url;

    }

    public function gerarSql()
    {
        $sql = "INSERT INTO wp_posts (ID, post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count) VALUES(";
        $sql .= "'" . $this->id . "'";
        $sql .= ", '" . $this->post_author . "'";
        $sql .= ", '" . $this->post_date . "'";
        $sql .= ", '" . $this->post_date_gmt . "'";
        $sql .= ", '" . $this->post_content . "'";
        $sql .= ", '" . $this->post_title . "'";
        $sql .= ", ''";
        $sql .= ", '" . $this->post_status . "'";
        $sql .= ", 'closed'";
        $sql .= ", 'closed'";
        $sql .= ", ''";
        $sql .= ", '" . $this->post_name . "'";
        $sql .= ", ''";
        $sql .= ", ''";
        $sql .= ", '" . $this->post_modified . "'";
        $sql .= ", '" . $this->post_modified_gmt . "'";
        $sql .= ", ''";
        $sql .= ", '0'";
        $sql .= ", '" . self::obterUrlPadrao() . "/?post_type=" . $this->post_type . "&#038;p=" . $this->id . "'";
        $sql .= ", '0'";
        $sql .= ", '" . $this->post_type . "'";
        $sql .= ", '" . $this->post_mime_type . "'";
        $sql .= ", '0');";

        return $sql;
    }

    public function limparAcentos($str)
    {
        $acentos = ['#', '@', '%', '¨', '&', '*', '(', ')', '!', '?', ':', ';', '<', '>', '|', '\\', '/', '“', '"', "'", '”', '$', ':', '+', '=', ' ', '°', 'º', 'ª', ',', '§', '--', 'ñ', 'ç', 'á', 'à', 'â', 'ã', 'é', 'è', 'ê', 'í', 'ì', 'î', 'ó', 'ò', 'ô', 'õ', 'ú', 'ù', 'û', 'ä', 'ë', 'ï', 'ö', 'ü', 'Ñ', 'Ç', 'Á', 'À', 'Â', 'Ã', 'É', 'È', 'Ê', 'Í', 'Ì', 'Î', 'Ó', 'Ò', 'Ô', 'Õ', 'Ú', 'Ù', 'Û', 'Ä', 'Ë', 'Ï', 'Ö', 'Ü'];
        $sub = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '-', 'n', 'c', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'a', 'e', 'i', 'o', 'u', 'N', 'C', 'A', 'A', 'A', 'A', 'E', 'E', 'E', 'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'A', 'E', 'I', 'O', 'U'];
        for ($i = 0; $i < count($acentos); $i++) {
            $str = str_replace($acentos[$i], $sub[$i], $str);
        }
        $str = str_replace('.','',$str);

        return strtolower($str);
    }
}