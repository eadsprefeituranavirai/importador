<?php

namespace App\Model;

use \PDO;
use Importador\Database\Connection;

class PodsRel
{

    private $pod_id = CONTRATOS;
    private $field_id = ANEXOS_CONTRATOS;

    private $id;
    private $item_id;
    private $related_pod_id = 0;
    private $related_field_id = 0;
    private $related_item_id = 0;
    private $weight = 0;

    public function __construct($post, $anexo)
    {
        $this->item_id = $post;
        $this->related_item_id = $anexo;
        $this->id = self::getLastId() + 1;

    }


    public static function getLastId()
    {
        $sql = 'SELECT max(id) as max FROM wp_podsrel';
        $conn = Connection::getConnectionWP();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        return $data->max;
    }

    function gerarSql()
    {
        $sql = "INSERT INTO wp_podsrel (id, pod_id, field_id, item_id, related_pod_id, related_field_id, related_item_id, weight) VALUES (";
        $sql .= "'" . $this->id . "'";
        $sql .= ", '" . $this->pod_id . "'";
        $sql .= ", '" . $this->field_id . "'";
        $sql .= ", '" . $this->item_id . "'";
        $sql .= ", '0'";
        $sql .= ", '0'";
        $sql .= ", '" . $this->related_item_id . "'";
        $sql .= ", '0');";
        return $sql;
    }

    public function save()
    {
        $conn = Connection::getConnectionWP();
        return $conn->exec($this->gerarSql());
    }
}