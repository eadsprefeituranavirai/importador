<?php
/**
 * Created by PhpStorm.
 * User: vinicius
 * Date: 08/08/18
 * Time: 18:31
 */

namespace App\Model;

use \PDO;
use Importador\Database\Connection;

class Contrato extends Post
{
    private $numero;

    /**
     * @return mixed
     */
    public function getPostTitle()
    {
        return $this->post_title;
    }
    private $ano;
    private $tipo;

    public function __construct($titulo, $descricao, $autor, $post_date, $numero, $ano, $tipo)
    {
        parent::__construct($titulo, $descricao, $autor, $post_date, 'contrato');
        $this->post_status = 'publish';

        $this->setAno($ano);
        $this->setNumero($numero);
        $this->setTipo($tipo);

        $this->post_name  = $this->post_name.$numero.$ano;

        $this->addCustomizado([
            'numero' => $this->numero,
            '_numero' => 'field_5b522699953cf',

            'ano' => $this->ano,
            '_ano' => 'field_5b522705953d0',

            'tipo' => $this->tipo,
            '_tipo' => 'field_5b522729953d1'
        ]);
    }

    public function addCustomizado($chave, $valor = '')
    {
        if (is_array($chave)) {
            foreach ($chave as $item => $txt) {
                $mod = new PostMeta($this->id, $item, $txt);
                $mod->save();
            }
        } else {
            $mod = new PostMeta($this->id, $chave, $valor);
            $mod->save();
        }
    }

    public function addAnexo($url, $data)
    {

        $anexo = new Anexo($url, $this->id, $data, $this->post_author);
        $anexo->save();
    }


    public function setNumero($numero)
    {
        $this->numero = $numero;
    }


    public function setAno($ano)
    {
        switch ($ano) {
            case 2012:
                $this->ano = 1;
                break;
            case 2013:
                $this->ano = 2;
                break;
            case 2014:
                $this->ano = 3;
                break;
            case 2015:
                $this->ano = 4;
                break;
            case 2016:
                $this->ano = 5;
                break;
            case 2017:
                $this->ano = 6;
                break;
            case 2018:
                $this->ano = 7;
                break;
            case 2019:
                $this->ano = 8;
                break;
            case 2020:
                $this->ano = 9;
                break;
        }
    }

    public function setTipo($tipo)
    {
        switch ($tipo) {
            case 'contrato':
                $this->tipo = 1;
                break;
            case 'ata_registro':
                $this->tipo = 2;
                break;

        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



}