<?php

namespace Model;

include_once 'Model\Connect.php';

use \PDO;
use Model\Connect;

class Licitacao
{
    private $id;
    private $post_author;
    private $post_date;
    private $post_date_gmt;
    private $post_content;
    private $post_title;
    private $post_name;
    private $post_modified;
    private $post_modified_gmt;

    private $sequencia;
    private $ano;
    private $situacao;
    private $modalidade;

    public function __construct($titulo, $descricao, $autor, $post_date, $sequencia, $ano, $situacao, $modalidade)
    {
        $this->id = self::getLastId() + 1;
        $this->post_author = $autor;
        $this->post_date = $post_date;
        $this->post_title = $titulo;
        $this->post_content = $descricao;
        $this->post_name = Post::limparAcentos($titulo);
        $this->post_modified = date('Y-m-d H:i:s');
        date_default_timezone_set('UTC');
        $this->post_date_gmt = date('Y-m-d H:i:s', strtotime($this->post_date));
        $this->post_modified_gmt = date('Y-m-d H:i:s', strtotime($this->post_modified));
        date_default_timezone_set('America/Campo_Grande');
        $this->setAno($ano);
        $this->sequencia = $sequencia;
        $this->setSituacao($situacao);
        $this->setModalidade($modalidade);
    }

    public function save()
    {
        $sql = $this->gerarSql();
        $conn = Connect::getConnectionWP();
        $licitacao = $conn->exec($sql);

        $seq = new PostMeta($this->id, 'sequencia', $this->sequencia);
        $seq->save();
        $seq = new PostMeta($this->id, '_sequencia', 'field_5b50d4af6d6a1');
        $seq->save();

        $ano = new PostMeta($this->id, 'ano', $this->ano);
        $ano->save();
        $ano = new PostMeta($this->id, '_ano', 'field_5b4510b14a1f8');
        $ano->save();

        $sit = new PostMeta($this->id, 'situacao', $this->situacao);
        $sit->save();
        $sit = new PostMeta($this->id, '_situacao', 'field_5b4f555951bd1');
        $sit->save();

        $mod = new PostMeta($this->id, 'modalidade', $this->modalidade);
        $mod->save();
        $mod = new PostMeta($this->id, '_modalidade', 'field_5b450ec54a1f7');
        $mod->save();
    }

    public function setModalidade($modalidade)
    {
        switch ($modalidade) {
            case 'registro_de_preco':
                $this->modalidade = 1;
                break;
            case 'chamada_publica':
                $this->modalidade = 2;
                break;
            case 'concorrencia':
                $this->modalidade = 4;
                break;
            case 'convite':
                $this->modalidade = 5;
                break;
            case 'credenciamento':
                $this->modalidade = 6;
                break;
            case 'dispensa_justificativa':
                $this->modalidade = 7;
                break;
            case 'dispensa_limite':
                $this->modalidade = 8;
                break;
            case 'inexigibilidade':
                $this->modalidade = 9;
                break;
            case 'leilao':
                $this->modalidade = 10;
                break;
            case 'pregao_eletronico':
                $this->modalidade = 11;
                break;
            case 'pregao_presencial':
                $this->modalidade = 12;
                break;
            case 'tomada_de_precos':
                $this->modalidade = 13;
                break;

        }
    }

    public function setSituacao($situacao)
    {
        switch ($situacao) {
            case 'em_andamento':
                $this->situacao = 1;
                break;
            case 'encerrada':
                $this->situacao = 2;
                break;
            case 'suspenso':
                $this->situacao = 3;
                break;

        }
    }

    public function setAno($ano)
    {
        switch ($ano) {
            case 2008;
                $this->ano = 1;
                break;
            case 2009;
                $this->ano = 2;
                break;
            case 2010;
                $this->ano = 3;
                break;
            case 2011;
                $this->ano = 4;
                break;
            case 2012;
                $this->ano = 5;
                break;
            case 2013;
                $this->ano = 6;
                break;
            case 2014;
                $this->ano = 7;
                break;
            case 2015;
                $this->ano = 8;
                break;
            case 2016;
                $this->ano = 9;
                break;
            case 2017;
                $this->ano = 10;
                break;
            case 2018;
                $this->ano = 11;
                break;
        }
    }


    public static function getLastId()
    {
        $sql = "SELECT max(ID) as max FROM wp_posts";
        $conn = Connect::getConnectionWP();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        $conn = null;
        return $data->max;
    }

    public static function obterUrlPadrao()
    {
        $sql = "SELECT option_value as url FROM wp_options WHERE option_id=1";
        $conn = Connect::getConnectionWP();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        $conn = null;
        return $data->url;
    }

    public function gerarSql()
    {
        $sql = "INSERT INTO wp_posts (ID, post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count) VALUES(";
        $sql .= "'" . $this->id . "'";
        $sql .= ", '" . $this->post_author . "'";
        $sql .= ", '" . $this->post_date . "'";
        $sql .= ", '" . $this->post_date_gmt . "'";
        $sql .= ", '" . $this->post_content . "'";
        $sql .= ", '" . $this->post_title . "'";
        $sql .= ", ''";
        $sql .= ", 'publish'";
        $sql .= ", 'open'";
        $sql .= ", 'closed'";
        $sql .= ", ''";
        $sql .= ", '".$this->post_name."'";
        $sql .= ", ''";
        $sql .= ", ''";
        $sql .= ", '" . $this->post_modified . "'";
        $sql .= ", '" . $this->post_modified_gmt . "'";
        $sql .= ", ''";
        $sql .= ", '0'";
        $sql .= ", '" . self::obterUrlPadrao() . "/?post_type=licitacao&#038;p=" . $this->id . "'";
        $sql .= ", '0'";
        $sql .= ", 'licitacao'";
        $sql .= ", ''";
        $sql .= ", '0');";

        return $sql;
    }
}