<?php

namespace Model;

include_once 'Model\Connect.php';

use \PDO;
use Model\Connect;

class Attachment
{
    private $url;
    private $id = null;
    private $post_author;
    private $post_date;
    private $post_date_gmt;
    private $post_content;
    private $post_title;
    private $post_excerpt;
    private $post_status;
    private $comment_status;
    private $ping_status;
    private $post_password;
    private $post_name;
    private $to_ping;
    private $pinged;
    private $post_modified;
    private $post_modified_gmt;
    private $post_content_filtered;
    private $post_parent;
    private $guid;
    private $menu_order;
    private $post_type;
    private $post_mime_type;
    private $comment_count;

    public function __construct($titulo, $conteudo, $data, $nome, $guid, $tipo, $parente, $url)
    {
        $this->id = null;
        $this->post_author = 10;
        $this->post_date = $data;
        $this->post_date_gmt = null;
        $this->post_content = $conteudo;
        $this->post_title = $titulo;
        $this->post_excerpt = null;
        $this->post_status = 'inherit';
        $this->comment_status = 'open';
        $this->ping_status = 'closed';
        $this->post_password = null;
        $this->post_name = $nome;
        $this->to_ping = null;
        $this->pinged = null;
        $this->post_modified = date('Y-m-d H:i:s');
        date_default_timezone_set('UTC');
        $this->post_modified_gmt = date('Y-m-d H:i:s', strtotime($this->post_modified));
        date_default_timezone_set('America/Campo_Grande');
        $this->post_content_filtered = null;
        $this->post_parent = $parente;
        $this->guid = $guid;
        $this->menu_order = '0';
        $this->post_type = 'attachment';
        $this->post_mime_type = $tipo;
        $this->comment_count = '0';
        $this->url = $url;
    }

    public function getLastId()
    {
        $sql = "SELECT max(ID) as max FROM wp_posts";
        $conn = Connect::getConnectionWP();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        return $data->max;
    }

    public function save()
    {
        $this->id = $this->getLastId() + 1;
        $sql = "INSERT INTO wp_posts (
        ID,
        post_author,
        post_date,
        post_date_gmt,
        post_content,
        post_title,
        post_excerpt,
        post_status,
        comment_status,
        ping_status,
        post_password,
        post_name,
        to_ping,
        pinged,
        post_modified,
        post_modified_gmt,
        post_content_filtered,
        post_parent,
        guid,
        menu_order,
        post_type,
        post_mime_type,
        comment_count
    ) VALUES (";
        $sql = $sql . "'" . $this->id . "'" . ', ';
        $sql = $sql . "'" . $this->post_author . "', ";
        $sql = $sql . "'" . $this->post_date . "', ";
        $sql = $sql . "'" . $this->post_date_gmt . "', ";
        $sql = $sql . "'" . $this->post_content . "', ";
        $sql = $sql . "'" . $this->post_title . "', ";
        $sql = $sql . "'" . $this->post_excerpt . "', ";
        $sql = $sql . "'" . $this->post_status . "', ";
        $sql = $sql . "'" . $this->comment_status . "', ";
        $sql = $sql . "'" . $this->ping_status . "', ";
        $sql = $sql . "'" . $this->post_password . "', ";
        $sql = $sql . "'" . $this->post_name . "', ";
        $sql = $sql . "'" . $this->to_ping . "', ";
        $sql = $sql . "'" . $this->pinged . "', ";
        $sql = $sql . "'" . $this->post_modified . "', ";
        $sql = $sql . "'" . $this->post_modified_gmt . "', ";
        $sql = $sql . "'" . $this->post_content_filtered . "', ";
        $sql = $sql . "'" . $this->post_parent . "', ";
        $sql = $sql . "'" . $this->guid . "', ";
        $sql = $sql . "'" . $this->menu_order . "', ";
        $sql = $sql . "'" . $this->post_type . "', ";
        $sql = $sql . "'" . $this->post_mime_type . "', ";
        $sql = $sql . "'" . $this->comment_count . "');";

        $conn = Connect::getConnectionWP();
        $result = $conn->exec($sql);

        $fonteMeta = new PostMeta($this->id, '_wp_attached_file', $this->url);
        $fonteMeta->save();

        $aux = str_replace('==imagem==', $this->url, PostMeta::METADATA_IMAGENS_DESTAQUE);
        $fonteMeta = new PostMeta($this->id, '_wp_attachment_metadata', str_replace('==tipo==', $this->post_mime_type, $aux));
        $fonteMeta->save();
        
        return $result;
    }

    public function setPostDate($post_date)
    {
        $this->post_date = $post_date;
    }

    public function setPostContent($post_content)
    {
        $this->post_content = $post_content;
    }

    public function setPostTitle($post_title)
    {
        $this->post_title = $post_title;
    }

    public function setPostName($post_name)
    {
        $this->post_name = $post_name;
    }

    public function setGuid($guid)
    {
        $this->guid = $guid;
    }

    public function setPostMimeType($post_mime_type)
    {
        $this->post_mime_type = $post_mime_type;
    }

    public function getPostMimeType()
    {
        return $this->post_mime_type;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getGuid()
    {
        return $this->guid;
    }

}