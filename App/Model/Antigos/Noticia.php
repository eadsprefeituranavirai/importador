<?php

namespace Model;

include_once 'Model\Connect.php';

use \PDO;
use Model\Connect;

class Noticia
{
    private $id;
    private $post_author;
    private $post_date;
    private $post_date_gmt;
    private $post_content;
    private $post_title;
    private $post_excerpt;
    private $post_status;
    private $comment_status;
    private $ping_status;
    private $post_password;
    private $post_name;
    private $to_ping;
    private $pinged;
    private $post_modified;
    private $post_modified_gmt;
    private $post_content_filtered;
    private $post_parent;
    private $guid;
    private $menu_order;
    private $post_type;
    private $post_mime_type;
    private $comment_count;

    public function __construct($titulo, $conteudo, $data, $nome)
    {
        $this->id = null;
        $this->post_author = 10;
        $this->post_date = $data;
        date_default_timezone_set('UTC');
        $this->post_date_gmt = date('Y-m-d H:i:s', strtotime($this->post_date));
        date_default_timezone_set('America/Campo_Grande');
        $this->post_content = $conteudo;
        $this->post_title = $titulo;
        $this->post_excerpt = null;
        $this->post_status = 'publish';
        $this->comment_status = 'closed';
        $this->ping_status = 'closed';
        $this->post_password = null;
        $this->post_name = $nome;
        $this->to_ping = null;
        $this->pinged = null;
        $this->post_modified = date('Y-m-d H:i:s');
        date_default_timezone_set('UTC');
        $this->post_modified_gmt = date('Y-m-d H:i:s', strtotime($this->post_modified));
        date_default_timezone_set('America/Campo_Grande');
        $this->post_content_filtered = null;
        $this->post_parent = 0;
        $this->guid = null;
        $this->menu_order = '0';
        $this->post_type = 'noticia';
        $this->post_mime_type = null;
        $this->comment_count = '0';
    }

    public static function getLastId()
    {
        $sql = "SELECT max(ID) as max FROM wp_posts";
        $conn = Connect::getConnectionWP();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        $conn = null;
        return $data->max;
    }

    public function save()
    {
        $this->id = self::getLastId() + 1;
        $this->guid = $noticiaUrl = 'http://10.0.0.223/wordpress/?post_type=noticia&#038;p=' . $this->id;
        $sql = "INSERT INTO wp_posts (
        ID,
        post_author,
        post_date,
        post_date_gmt,
        post_content,
        post_title,
        post_excerpt,
        post_status,
        comment_status,
        ping_status,
        post_password,
        post_name,
        to_ping,
        pinged,
        post_modified,
        post_modified_gmt,
        post_content_filtered,
        post_parent,
        guid,
        menu_order,
        post_type,
        post_mime_type,
        comment_count
    ) VALUES (";
        $sql = $sql . "'" . $this->id . "'" . ', ';
        $sql = $sql . "'" . $this->post_author . "', ";
        $sql = $sql . "'" . $this->post_date . "', ";
        $sql = $sql . "'" . $this->post_date_gmt . "', ";
        $sql = $sql . "'" . $this->post_content . "', ";
        $sql = $sql . "'" . $this->post_title . "', ";
        $sql = $sql . "'" . $this->post_excerpt . "', ";
        $sql = $sql . "'" . $this->post_status . "', ";
        $sql = $sql . "'" . $this->comment_status . "', ";
        $sql = $sql . "'" . $this->ping_status . "', ";
        $sql = $sql . "'" . $this->post_password . "', ";
        $sql = $sql . "'" . $this->post_name . "', ";
        $sql = $sql . "'" . $this->to_ping . "', ";
        $sql = $sql . "'" . $this->pinged . "', ";
        $sql = $sql . "'" . $this->post_modified . "', ";
        $sql = $sql . "'" . $this->post_modified_gmt . "', ";
        $sql = $sql . "'" . $this->post_content_filtered . "', ";
        $sql = $sql . "'" . $this->post_parent . "', ";
        $sql = $sql . "'" . $this->guid . "', ";
        $sql = $sql . "'" . $this->menu_order . "', ";
        $sql = $sql . "'" . $this->post_type . "', ";
        $sql = $sql . "'" . $this->post_mime_type . "', ";
        $sql = $sql . "'" . $this->comment_count . "');";
        $conn = Connect::getConnectionWP();


        print ('<h2>');
        print ($this->id);
        print(' concluído com sucesso!</h2>');
        print '<br>';
        $result = $conn->exec($sql);


        if ($result){
            echo $this->id.' concluído com sucesso!';
        }
        $fonteMeta = new PostMeta($this->id, 'fonte', 'Assessoria de Imprensa');
        $fonteMeta->save();

        return $result;
    }


}