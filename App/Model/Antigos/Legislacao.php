<?php

namespace Model;

include_once 'Model\Connect.php';

use \PDO;
use Model\Connect;

class Legislacao
{
    private $id;//Tratado
    private $post_author;//Tratado
    private $post_date;//Tratado
    private $post_date_gmt;//Tratado
    private $post_content;//Tratado
    private $post_title;//Tratado
    private $post_name;//Tratado
    private $post_modified;//Tratado
    private $post_modified_gmt;//Tratado

    private $numero;
    private $ano;
    private $categoria;

    public function __construct($titulo, $descricao, $autor, $post_date, $numero, $ano, $categoria)
    {
        $this->id = self::getLastId() + 1;
        $this->post_author = $autor;
        $this->post_date = $post_date;
        $this->post_title = $titulo;
        $this->post_content = $descricao;
        $this->post_name = Post::limparAcentos($titulo.$ano.$numero);
        $this->post_modified = date('Y-m-d H:i:s');
        date_default_timezone_set('UTC');
        $this->post_date_gmt = date('Y-m-d H:i:s', strtotime($this->post_date));
        $this->post_modified_gmt = date('Y-m-d H:i:s', strtotime($this->post_modified));
        date_default_timezone_set('America/Campo_Grande');
        $this->setAno($ano);

        $this->numero = $numero;
        $this->setCategoria($categoria);
    }

    public function save()
    {
        $sql = $this->gerarSql();
        $conn = Connect::getConnectionWP();
        $licitacao = $conn->exec($sql);

        $seq = new PostMeta($this->id, 'categoria', $this->categoria);
        $seq->save();
        $seq = new PostMeta($this->id, '_categoria', 'field_5b52219c4e0df');
        $seq->save();

        $ano = new PostMeta($this->id, 'numero', $this->numero);
        $ano->save();
        $ano = new PostMeta($this->id, '_numero', 'field_5b5222404e0e0');
        $ano->save();

        $mod = new PostMeta($this->id, 'ano', $this->ano);
        $mod->save();

        $mod = new PostMeta($this->id, '_ano', 'field_5b5222714e0e1');
        $mod->save();


    }

    public function setCategoria($categoria)
    {
        switch ($categoria) {
            case 'leis_complementares':
                $this->categoria = 1;
                break;
            case 'leis_ordinarias':
                $this->categoria = 2;
                break;
            case 'decretos':
                $this->categoria = 3;
                break;
            case 'portarias':
                $this->categoria = 4;
                break;
        }
    }

    public function setSituacao($situacao)
    {
        switch ($situacao) {
            case 'em_andamento':
                $this->situacao = 1;
                break;
            case 'encerrada':
                $this->situacao = 2;
                break;
            case 'suspenso':
                $this->situacao = 3;
                break;

        }
    }

    public function setAno($ano)
    {
        switch ($ano) {
            case 1974;
                $this->ano = 1;
                break;
            case 1997;
                $this->ano = 2;
                break;
            case 1998;
                $this->ano = 3;
                break;
            case 1999;
                $this->ano = 4;
                break;
            case 2000;
                $this->ano = 5;
                break;
            case 2001;
                $this->ano = 6;
                break;
            case 2003;
                $this->ano = 7;
                break;
            case 2004;
                $this->ano = 8;
                break;
            case 2005;
                $this->ano = 9;
                break;
            case 2006;
                $this->ano = 10;
                break;
            case 2007;
                $this->ano = 11;
                break;
            case 2008;
                $this->ano = 12;
                break;
            case 2009;
                $this->ano = 13;
                break;
            case 2010;
                $this->ano = 14;
                break;
            case 2011;
                $this->ano = 15;
                break;
            case 2012;
                $this->ano = 16;
                break;
            case 2013;
                $this->ano = 17;
                break;
            case 2014;
                $this->ano = 18;
                break;
            case 2015;
                $this->ano = 19;
                break;
            case 2016;
                $this->ano = 20;
                break;
            case 2017;
                $this->ano = 21;
                break;
            case 2018;
                $this->ano = 22;
                break;
        }
    }


    public static function getLastId()
    {
        $sql = "SELECT max(ID) as max FROM wp_posts";
        $conn = Connect::getConnectionWP();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        $conn = null;
        return $data->max;
    }

    public static function obterUrlPadrao()
    {
        $sql = "SELECT option_value as url FROM wp_options WHERE option_id=1";
        $conn = Connect::getConnectionWP();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        $conn = null;
        return $data->url;

    }

    public function gerarSql()
    {
        $sql = "INSERT INTO wp_posts (ID, post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count) VALUES(";
        $sql .= "'" . $this->id . "'";
        $sql .= ", '" . $this->post_author . "'";
        $sql .= ", '" . $this->post_date . "'";
        $sql .= ", '" . $this->post_date_gmt . "'";
        $sql .= ", '" . $this->post_content . "'";
        $sql .= ", '" . $this->post_title . "'";
        $sql .= ", ''";
        $sql .= ", 'publish'";
        $sql .= ", 'open'";
        $sql .= ", 'closed'";
        $sql .= ", ''";
        $sql .= ", '" . $this->post_name . "'";
        $sql .= ", ''";
        $sql .= ", ''";
        $sql .= ", '" . $this->post_modified . "'";
        $sql .= ", '" . $this->post_modified_gmt . "'";
        $sql .= ", ''";
        $sql .= ", '0'";
        $sql .= ", '" . self::obterUrlPadrao() . "/?post_type=legislacao_municipal&#038;p=" . $this->id . "'";
        $sql .= ", '0'";
        $sql .= ", 'legislacao_municipal'";
        $sql .= ", ''";
        $sql .= ", '0');";
        return $sql;
    }
}