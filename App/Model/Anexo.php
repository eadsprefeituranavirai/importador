<?php

namespace App\Model;

use \PDO;
use Importador\Database\Connection;

class Anexo
{
    private $id = null;
    private $post_author;
    private $post_date;
    private $post_date_gmt;
    private $post_content;
    private $post_title;
    private $post_name;
    private $post_modified;
    private $post_modified_gmt;
    private $post_parent;
    private $post_mime_type = '';
    private $guid;

    public function __construct($url, $parent, $data, $autor)
    {
        $titulo = substr($url, strrpos($url, '/') + 1);
        $descricao = str_replace('-', '-', $titulo);
        $descricao = str_replace(' ', '', $descricao);
        $test = array(
            "/(á|à|ã|â|ä|Á|À|Ã|Â|Ä)/",
            "/(é|è|ê|ë|É|È|Ê|Ë)/",
            "/(í|ì|î|ï|Í|Ì|Î|Ï)/",
            "/(ó|ò|õ|ô|ö|Ó|Ò|Õ|Ô|Ö)/",
            "/(ú|ù|û|ü|Ú|Ù|Û|Ü)/",
            "/(ñ|Ñ)/",
            "/(ç|Ç)/",
            "/('|#|@|%|¨|&|!|:|;|<|>|“|\"|”|$|:|=|°|º|ª|,|§)/");
        $test2 = array("a", "e", "i", "o", "u", "n", "c", " ");
        $descricao = preg_replace($test, $test2, $descricao);
        $descricao = strtolower($descricao);
        $descricao = str_replace(' ', '', $descricao);

        $url = str_replace($titulo, '', $url) . $descricao;

        $arq = str_replace(substr($url, strrpos($url, '.')), '', $url);
        $nome = Post::limparAcentos(substr($arq, strrpos($arq, '/') + 1));
        $this->post_parent = $parent;
        $this->guid = $url;
        $this->id = self::getLastId() + 1;
        $this->post_author = $autor;
        $this->post_date = date('Y-m-d H:i:s', strtotime($data));
        $this->post_title = $titulo;
        $this->post_content = $titulo;
        $this->post_name = $nome . $this->id;
        $this->post_modified = date('Y-m-d H:i:s');
        date_default_timezone_set('UTC');
        $this->post_date_gmt = date('Y-m-d H:i:s', strtotime($this->post_date));
        $this->post_modified_gmt = date('Y-m-d H:i:s', strtotime($this->post_modified));
        date_default_timezone_set('America/Campo_Grande');
        if (substr($url, strrpos($url, '.')) == '.pdf') {
            $this->post_mime_type = 'application/pdf';
        }
    }

    public static function getLastId()
    {
        $sql = "SELECT max(ID) as max FROM wp_posts";
        $conn = Connection::getConnectionWP();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        $conn = null;
        return $data->max;
    }

    public static function obterUrlPadrao()
    {
        $sql = "SELECT option_value as url FROM wp_options WHERE option_id=1";
        $conn = Connection::getConnectionWP();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        $conn = null;
        return $data->url;
    }

    public function gerarSql()
    {
        $sql = "INSERT INTO wp_posts (ID, post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count) VALUES(";
        $sql .= "'" . $this->id . "'";
        $sql .= ", '" . $this->post_author . "'";
        $sql .= ", '" . $this->post_date . "'";
        $sql .= ", '" . $this->post_date_gmt . "'";
        $sql .= ", '" . $this->post_content . "'";
        $sql .= ", '" . $this->post_title . "'";
        $sql .= ", ''";
        $sql .= ", 'inherit'";
        $sql .= ", 'open'";
        $sql .= ", 'closed'";
        $sql .= ", ''";
        $sql .= ", '" . $this->post_name . "'";
        $sql .= ", ''";
        $sql .= ", ''";
        $sql .= ", '" . $this->post_modified . "'";
        $sql .= ", '" . $this->post_modified_gmt . "'";
        $sql .= ", ''";
        $sql .= ", '" . $this->post_parent . "'";
        $sql .= ", '" . self::obterUrlPadrao() . "/wp-content/uploads/site_antigo/" . $this->guid . "'";
        $sql .= ", '0'";
        $sql .= ", 'attachment'";
        $sql .= ", '" . $this->post_mime_type . "'";
        $sql .= ", '0');";

        return $sql;
    }

    public function save()
    {
        $sql = $this->gerarSql();
        $conn = Connection::getConnectionWP();
        $licitacao = $conn->exec($sql);

        $seq = new PostMeta($this->id, '_wp_attached_file', 'site_antigo/' . $this->guid);
        $seq->save();

        $rel = new PodsRel($this->post_parent, $this->id);
        $rel->save();

    }

}