<?php

namespace App\Control;

use Importador\Database\Connection;
use \PDO;


class ControllerNoticia  extends Controller
{
    public function show(){
        parent::show();

        $sql = 'SELECT COUNT(id) as qtd FROM artigo_artigos';
        $conn = Connection::getConnectionTalsk();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        $conn = null;

        $this->template->mostrar([
            'titulo' => 'Notícias',
            'subTitulo' => 'Importador de Notícias do Talski CMS para Wordpress',
            'total' => $data->qtd,
        ]);
    }
}