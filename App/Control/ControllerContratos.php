<?php

namespace App\Control;

use Importador\Database\Connection;
use \PDO;

class ControllerContratos extends Controller
{
    public function show(){
        parent::show();

        $sql = 'SELECT COUNT(id) as qtd FROM licitacao_contratos_convenios';
        $conn = Connection::getConnectionTalsk();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        $conn = null;

        $this->template->mostrar([
            'titulo' => 'Contratos e Atas',
            'subTitulo' => 'Importador de Contratos e Atas do Talski CMS para Wordpress',
            'total' => $data->qtd,
            'classe' => 'ControllerContratos',
            'metodo' => 'show'
        ]);
    }
}