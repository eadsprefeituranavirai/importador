<?php

namespace App\Control\Api;

use \PDO;
use Importador\Database\Connection;
use App\Model\Contrato;

class ControllerContratos
{

    function show()
    {
        $conn = Connection::getConnectionTalsk();
        $sql = 'SELECT * FROM licitacao_contratos_convenios';
        $select = $conn->query($sql);
        $select = $select->fetchAll(PDO::FETCH_OBJ);

        $arr = array(
            'tipo' => 'contrato',
            'data' => $select
        );
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($arr);
    }

    function save($request){
        $titulo = $request->titulo;
        $descricao = $request->descricao;
        $autor = $request->autor;
        $data = $request->data;
        $numero = $request->numero;
        $ano = $request->ano;
        $tipo = $request->tipo;
        $anexos = $request->anexos;
        $contrato = new Contrato($titulo, $descricao, $autor, $data, $numero, $ano, $tipo, $anexos);
        $contrato->save();

        if (is_array($anexos)){
            foreach ($anexos as $anexo){
                $contrato->addAnexo($anexo->url, $anexo->data);
            }
        }
    }
}