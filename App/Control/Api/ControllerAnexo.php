<?php

namespace App\Control\Api;

use \PDO;
use Importador\Database\Connection;

class ControllerAnexo extends Controller
{
    public function show($request){
        $conn = Connection::getConnectionTalsk();
        $sql = "SELECT * FROM anexo_arquivos WHERE anexavel_type='Licitacao::ContratoConvenio' AND anexavel_id={$request['id']};";
        $select = $conn->query($sql);
        $select = $select->fetchAll(PDO::FETCH_OBJ);

        $arr = array(
            'tipo' => 'attachment',
            'data' => $select
        );
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($arr);
    }
}