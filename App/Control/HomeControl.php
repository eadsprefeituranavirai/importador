<?php

namespace App\Control;

use Importador\Control\Page;
use App\Templates\HomeTemplate;
use Config;
use \PDO;

class HomeControl extends Page {

    public function show($msg = null)
    {
        $tem = new HomeTemplate();
        $tem->mostrar([
            'titulo' => 'Configurar Importador',
            'subTitulo' => 'Definir as bases de dados a serem acessadas',
            'bancoTalski' => Config::carregarBancoTalski(),
            'bancoWṕ' => Config::carregarBancoWp(),
            'successMessage' => $msg,
        ]);
    }
}