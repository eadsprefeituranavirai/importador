<?php

namespace App\Control;

use Importador\Database\Connection;
use \PDO;

class ControllerRGF  extends Controller
{
    public function show(){
        parent::show();

        $sql = 'SELECT COUNT(id) as qtd FROM contabilidade_rgfs';
        $conn = Connection::getConnectionTalsk();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        $conn = null;

        $this->template->mostrar([
            'titulo' => 'RGF',
            'subTitulo' => 'Importador de RGF\'s do Talski CMS para Wordpress',
            'total' => $data->qtd,
        ]);
    }
}