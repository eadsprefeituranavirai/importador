<?php

namespace App\Control;

use Importador\Database\Connection;
use \PDO;

class ControllerLeis extends Controller
{
    public
    function show()
    {
        parent::show();

        $sql = 'SELECT COUNT(id) as qtd FROM legislacao_municipais';
        $conn = Connection::getConnectionTalsk();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        $conn = null;

        $this->template->mostrar([
            'titulo' => 'Leis',
            'subTitulo' => 'Importador de Leis do Talski CMS para Wordpress',
            'total' => $data->qtd,
        ]);
    }
}