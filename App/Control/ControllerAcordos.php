<?php

namespace App\Control;

use Importador\Database\Connection;
use \PDO;

class ControllerAcordos extends Controller
{
    public function show(){
        parent::show();

        $sql = 'SELECT COUNT(id) as qtd FROM termo_colaboracoes_fomentos';
        $conn = Connection::getConnectionTalsk();
        $result = $conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        $conn = null;

        $this->template->mostrar([
            'titulo' => 'Termos e Acordos',
            'subTitulo' => 'Importador de Termos e Acordos do Talski CMS para Wordpress',
            'total' => $data->qtd,
        ]);
    }
}