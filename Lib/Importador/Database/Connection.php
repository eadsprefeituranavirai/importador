<?php

namespace Importador\Database;

use \PDO;
use Exception;

final class Connection{
	private static $connectionWP;
	private static $connectionTalsk;

	private function __construct(){}

	public static function getConnectionWP(){
		if (isset(self::$connectionWP)) {
			return self::$connectionWP;
		}else{
//			self::$connectionWP = new PDO("mysql:host=wpnavirai.mysql.dbaas.com.br;dbname=wpnavirai;charset=utf8", "wpnavirai", "Gajerovi@#2018");
			self::$connectionWP = new PDO("mysql:host=".HOST_WP.";dbname=".NOME_WP.";charset=utf8", USER_WP, SENHA_WP);
		}
		return self::$connectionWP;
	}

	public static function getConnectionTalsk(){
		if (isset(self::$connectionTalsk)) {
			return self::$connectionTalsk;
		}else{
//            self::$connectionTalsk = new PDO("mysql:host=10.0.0.205;dbname=navirai;charset=utf8", "suporte", "@#1234");
            self::$connectionTalsk = new PDO("mysql:host=".HOST_TALSKI.";dbname=".NOME_TALSKI.";charset=utf8", USER_TALSKI, SENHA_TALSKI);
		}
		return self::$connectionTalsk;
	}

}