<?php

require_once './Config.php';

use App\Control\HomeControl;
use App\Control\ControllerContratos;
use App\Control\ControllerNoticia;


if ($_GET) {
    if ($_SERVER['PATH_INFO'] == '/api/') {
        $class = "App\\Control\\Api\\" . $_GET['class'];
        if (class_exists($class)) {
            $pagina = new $class;
            if (method_exists($pagina, $_GET['method'])) {
                $method = $_GET['method'];
                call_user_func(array($pagina, $method), $_GET);
            }
        }
    } else {
        $class = "App\\Control\\" . $_GET['class'];
        if (class_exists($class)) {
            $pagina = new $class;
            $pagina->show();
        }
    }

} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['salvar_config'])) {
        Config::salvar(array(
            'bancoTalski' => [
                'nomeTalski' => $_POST['nomeTalski'],
                'hostTalski' => $_POST['hostTalski'],
                'userTalski' => $_POST['userTalski'],
                'senhaTalski' => $_POST['senhaTalski']],
            'bancoWp' => [
                'nomeWp' => $_POST['nomeWp'],
                'hostWp' => $_POST['hostWp'],
                'userWp' => $_POST['userWp'],
                'senhaWp' => $_POST['senhaWp']],
        ));
    } else {
        $obj = json_decode(file_get_contents('php://input'));

        $class = $obj->class;
        $method = $obj->method;

        unset($obj->class);
        unset($obj->method);

        $class = "App\\Control\\Api\\" . $class;

        if (class_exists($class)) {
            $pagina = new $class;
            if (method_exists($pagina, $method)) {
                call_user_func(array($pagina, $method), $obj);
            }
        }
    }
} else {
    $pg = new HomeControl();
    $pg->show();
}
