<!doctype HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
include_once 'Model\Attachment.php';
include_once 'Model\Licitacao.php';
include_once 'Model\Connect.php';
include_once 'Model\PostMeta.php';
include_once 'Model\Anexo.php';
include_once 'Model\PodsRel.php';


use Model\Attachment;
use Model\Noticia;
use Model\Connect;
use Model\Anexo;
use Model\PostMeta;
use Model\PodsRel;


$sql = "SELECT option_value FROM wp_options WHERE option_id=1";
$conn = Connect::getConnectionWP();
$result = $conn->query($sql);
$data = $result->fetch(PDO::FETCH_OBJ);
$localizacao = $data->option_value;
$conn = null;
$responsavel = 13;

echo 'Importando licitações!';


$conn = Connect::getConnectionTalsk();

$sql = 'SELECT * FROM licitacao_licitacoes ORDER BY ID DESC';
$select = $conn->query($sql);
$select = $select->fetchAll(PDO::FETCH_OBJ);
$quantidade = 0;
$quantidadeSemAnexo = 0;
set_time_limit(0);
foreach ($select as $item) {
    $titulo = $item->titulo;
    $descricao = $item->descricao;
    $autor = $responsavel;
    $data = date('Y-m-d H:i:s', strtotime($item->publicacao));
    $sequencia = $item->numero_sequencia;
    $ano = $item->numero_ano;
    $situacao = $item->situacao;
    $modalidade = $item->modalidade;

    $licitacao = new \Model\Licitacao($titulo, $descricao, $autor, $data, $sequencia, $ano, $situacao, $modalidade);
    $licitacao->save();

    $sql = "SELECT * FROM anexo_arquivos where anexavel_id=" . $item->id . " and anexavel_type='Licitacao::Licitacao';";
    $talski = Connect::getConnectionTalsk();
    $result = $talski->query($sql);
    $data = $result->fetch(PDO::FETCH_OBJ);

    if (is_object($data)) {
        $url = $data->arquivo_uid;
        $parent = \Model\Licitacao::getLastId();
        $publicacao = $data->created_at;
        $autor = $responsavel;
        $anexar = new \Model\Anexo($url, $parent, $publicacao, $autor);
        $anexar->save();
    } else {
        if (is_array($data)) {
            foreach ($data as $anexo) {
                $url = $anexo->arquivo_uid;
                $parent = \Model\Licitacao::getLastId();
                $publicacao = $anexo->created_at;
                $autor = $responsavel;
                $anexar = new \Model\Anexo($url, $parent, $publicacao, $autor);

                $anexar->save();
            }
        }else{
            $quantidadeSemAnexo++;
        }
    }
    $quantidade++;
}
echo '<br><br><br><br><br><br><br><br>';
echo "<center><b><h1>Concluido com sucesso!</h1><br><h3>{$quantidade} licitacoes transferidas<br>onde {$quantidadeSemAnexo} nao tem arquivo anexado!</h3></b></center>"

?>
</body>
</html>