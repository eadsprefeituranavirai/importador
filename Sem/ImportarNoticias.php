<!doctype HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
include_once 'Model\Attachment.php';
include_once 'Model\Noticia.php';
include_once 'Model\Connect.php';
include_once 'Model\PostMeta.php';


use Model\Attachment;
use Model\Noticia;
use Model\Connect;
use Model\PostMeta;

$sql = "SELECT option_value FROM wp_options WHERE option_id=1";
$conn = Connect::getConnectionWP();
$result = $conn->query($sql);
$data = $result->fetch(PDO::FETCH_OBJ);
$localizacao = $data->option_value;
$conn = null;


$conn = Connect::getConnectionTalsk();


$sql = 'SELECT * FROM artigo_artigos WHERE id < 5 ORDER BY ID DESC';

$select = $conn->query($sql);
$select = $select->fetchAll(PDO::FETCH_OBJ);

foreach ($select as $item) {
    $texto = limparTexto($item->texto);
    $noticiaTitulo = $item->titulo;
    $noticiaConteudo = $texto;
    $noticiaData = $item->publicacao;
    $noticiaNome = limparNome($noticiaTitulo);
    $noticia = new Noticia($noticiaTitulo, $noticiaConteudo, $noticiaData, $noticiaNome);
    $noticia->save();

    $id = $item->id;
    $texto = $item->texto;
    $foto_id = 0;
    if (strpos($texto, '{foto :id => ') >= 0) {
        $foto_id = str_replace(' ', '', substr($texto, strpos($texto, '{foto :id =>'), 18));
        $foto_id = substr($foto_id, 0, strrpos($foto_id, '}'));
        $foto_id = str_replace('{foto:id=>', '', $foto_id);
        $foto_id = intval($foto_id);
    }
    if ($foto_id == 0) {
        $sql = 'SELECT * FROM artigo_destaques WHERE artigo_id=' . $id;
        $destaque = $conn->query($sql);
        $destaque = $destaque->fetchAll(PDO::FETCH_OBJ);
        foreach ($destaque as $dest) {
            $foto_id = $foto_id = str_replace(' ', '', substr($texto, strpos($texto, '{foto :id =>'), 18));
            $foto_id = str_replace(' ', '', $dest->texto);
            $foto_id = substr($foto_id, 0, strrpos($foto_id, '}'));
            $foto_id = str_replace('{foto:id=>', '', $foto_id);
            $foto_id = intval($foto_id);
        }
    }
    $sql = 'SELECT * FROM foto_fotos WHERE id=' . $foto_id;

    $fotos = $conn->query($sql);
    $fotos = $fotos->fetchAll(PDO::FETCH_OBJ);

    foreach ($fotos as $foto) {
        if (isset($foto->arquivo_name)) {
            $nome = $foto->arquivo_name;
            if (!strpos($foto->arquivo_uid, $nome)) {
                $nome = str_replace('.jpg', '.jpeg', $nome);
            }
            if (!strpos($foto->arquivo_uid, $nome)) {
                $nome = str_replace('.JPG', '.jpeg', $nome);
            }
            if (!strpos($foto->arquivo_uid, $nome)) {
                $nome = str_replace('.jpg', '.png', $nome);
            }
            if (strpos($foto->arquivo_uid, '.png') >= 0) {
                $imagemTipo = 'image/png';
            }
            if (strpos($foto->arquivo_uid, '.jpeg') >= 0 || strpos($item->arquivo_uid, '.jpg') >= 0) {
                $imagemTipo = 'image/jpeg';
            }

            $url = str_replace($nome, '', $foto->arquivo_uid);
            $nome = limparNome($nome);
            $foto_id = $localizacao . '/wp-content/uploads/site_antigo/' . $url . $nome;

            $url = 'site_antigo/' . $url . $nome;
            $imagemTitulo = $foto->legenda;
            $imagemConteudo = $nome;
            $imagemData = $foto->created_at;
            $imagemNome = $nome;
            $imagemUrl = $foto_id;
            $imagemParente = Noticia::getLastId();
            $imagem = new Attachment($imagemTitulo, $imagemConteudo, $imagemData, $imagemNome, $imagemUrl, $imagemTipo, $imagemParente, $url);
            $imagem->save();
        }
    }
}




function limparTexto($texto)
{
    $inicio = true;
    while ($inicio) {
        $inicio = strpos($texto, '{');
        if ($inicio === false) {
            $inicio = strpos($texto, '{foto :id=>');
        }

        if ($inicio >= 1 || $inicio === 0) {
            $fim = strpos($texto, '}');
            $fim = $fim - $inicio + 1;
            $remover = substr($texto, $inicio, $fim);
            $texto = str_replace($remover, '', $texto);
            $inicio = true;
        }
    }
    return $texto;
}

?>
</body>
</html>